// Command interpreter for testing the IMM link to the DPC
package main

import (
	"bitbucket.org/mfkenney/go-power"
	"bitbucket.org/uwaploe/go-imm"
	"flag"
	"fmt"
	"github.com/tarm/goserial"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"
)

var port = map[string]interface{}{
	"device": "/dev/ttymxc2",
	"baud":   9600,
	"switch": "131",
}

var settings = map[string]interface{}{
	"thost0":                  0,
	"thost1":                  0,
	"thost2":                  500,
	"thost3":                  1000,
	"tmodem2":                 600,
	"tmodem3":                 1100,
	"enableecho":              0,
	"enablehostwakeupcr":      0,
	"enablehostpromptconfirm": 0,
	"enablefullpwrtx":         1,
	"enablebackspace":         0,
	"termtohost":              254,
	"termfromhost":            254,
}

var Version = "dev"
var BuildDate = "unknown"

type ImmState struct {
	imm     *imm.IMM
	sw      power.PowerSwitch
	peer_sn string
	err     error
	cancel  chan struct{}
}

func shutdown(ms *ImmState) {
	ms.imm.Sleep()
	time.Sleep(time.Millisecond * 250)
	ms.sw.Off()
	log.Println("Power off")
}

func find_peer(ms *ImmState, interval, tries int) (status bool) {
	var peers []string
	var count int

	log.Println("Looking for peer IMM")
	peers, ms.err = ms.imm.FindPeers()
	if peers != nil {
		log.Printf("Found peer: %s", peers[0])
		ms.peer_sn = peers[0]
		status = true
	} else {
		count = 1
		// Run the peer search in a goroutine so it can be
		// interrupted.
		cs := make(chan bool)
		ticker := time.NewTicker(
			time.Duration(interval) * time.Millisecond)
		defer ticker.Stop()

		go func() {
			for _ = range ticker.C {
				count++
				log.Println("Attempt %d", count)
				peers, ms.err = ms.imm.FindPeers()
				if peers != nil {
					log.Printf("Found peer: %s", peers[0])
					ms.peer_sn = peers[0]
					cs <- true
					return
				}
				tries--
				if tries <= 0 {
					ms.err = fmt.Errorf("No peers found")
					cs <- false
					return
				}
			}
		}()

		select {
		case status = <-cs:
		case <-ms.cancel:
			ticker.Stop()
			status = false
		}
	}

	return
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options]\n", os.Args[0])
		fmt.Fprintf(os.Stderr, "Test IMM comms with the DPC\n\n")
		flag.PrintDefaults()
	}

	showvers := flag.Bool("version", false,
		"Show program version information and exit")

	flag.Parse()
	if *showvers {
		fmt.Printf("%s %s\n", os.Args[0], Version)
		fmt.Printf("  Build date: %s\n", BuildDate)
		fmt.Printf("  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	// Open serial port
	c := &serial.Config{Name: port["device"].(string),
		Baud: port["baud"].(int)}
	s, err := serial.OpenPort(c)
	if err != nil {
		panic(err)
	}

	// Power switch
	sw := power.NewTsPowerSwitch(port["switch"].(string))
	if sw == nil {
		log.Fatalf("Cannot access power switch via TSCTL")
	}
	// Make sure the power is off
	sw.Off()

	ms := &ImmState{
		imm:    imm.NewIMM(s),
		sw:     sw,
		cancel: make(chan struct{}, 1),
	}

	time.Sleep(time.Millisecond * 1000)
	sw.On()
	ms.imm.SetTimeout(time.Second * 5)
	defer shutdown(ms)

	time.Sleep(time.Millisecond * 2500)
	err = ms.imm.Wakeup()

	if err != nil {
		sw.Off()
		log.Fatal(err)
	}

	for k, v := range settings {
		err := ms.imm.Set(k, v)
		if err != nil {
			log.Fatal(err)
		}
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close. If a signal arrives, close the "cancel" channel
	// to cleanly shutdown the FSM.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			close(ms.cancel)
		}
	}()

	time.Sleep(time.Millisecond * 1000)
	status := find_peer(ms, 5000, 10)
	if status {
		run_shell(ms)
	} else {
		if ms.err != nil {
			log.Println(err)
		}
	}
}
