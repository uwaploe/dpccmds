package main

import (
	"bitbucket.org/uwaploe/go-dpcmsg"
	"encoding/base64"
	"fmt"
	"github.com/gobs/cmd"
	"github.com/gogo/protobuf/proto"
	"log"
	"strconv"
	"strings"
	"time"
)

func do_ping(args []string, p ...interface{}) (stop bool) {
	ms := p[0].(*ImmState)
	hostid, err := strconv.Atoi(ms.peer_sn)
	if err != nil {
		log.Fatal(err)
	}

	resp, err := ms.imm.HostMessage(hostid, "ping")
	if err != nil {
		log.Println(err)
	} else {
		parts := strings.SplitN(resp, ":", 2)
		if len(parts) > 1 {
			fmt.Println(strings.TrimRight(parts[1], " \r\n"))
		}
	}
	return
}

func do_reboot(args []string, p ...interface{}) (stop bool) {
	ms := p[0].(*ImmState)
	hostid, err := strconv.Atoi(ms.peer_sn)
	if err != nil {
		log.Fatal(err)
	}

	if len(args) == 0 {
		fmt.Println("Usage: reboot DURATION")
		return
	}

	delay, err := time.ParseDuration(args[0])
	if err == nil {
		resp, err := ms.imm.HostMessage(hostid,
			"reboot "+strconv.Itoa(int(delay.Seconds())))
		if err == nil {
			parts := strings.SplitN(resp, ":", 2)
			if len(parts) > 1 {
				fmt.Println(strings.TrimRight(parts[1], " \r\n"))
			}
		} else {
			log.Println(err)
		}
	} else {
		log.Println(err)
	}

	return
}

func do_batt(args []string, p ...interface{}) (stop bool) {
	ms := p[0].(*ImmState)
	hostid, err := strconv.Atoi(ms.peer_sn)
	if err != nil {
		log.Fatal(err)
	}

	resp, err := ms.imm.HostMessage(hostid, "batt")
	if err == nil {
		if len(args) > 0 && args[0] == "--raw" {
			fmt.Println(resp)
		} else {
			parts := strings.SplitN(resp, ":", 2)
			if len(parts) == 2 {
				switch parts[0] {
				case "B":
					process_battery(strings.TrimRight(parts[1], " \r\n"))
				case "I":
					fmt.Println(strings.TrimRight(parts[1], " \r\n"))
				}
			} else {
				log.Printf("Invalid response: %q", resp)
			}
		}
	} else {
		log.Println(err)
	}
	return
}

func do_status(args []string, p ...interface{}) (stop bool) {
	ms := p[0].(*ImmState)
	hostid, err := strconv.Atoi(ms.peer_sn)
	if err != nil {
		log.Fatal(err)
	}

	resp, err := ms.imm.HostMessage(hostid, "status")
	if err == nil {
		if len(args) > 0 && args[0] == "--raw" {
			fmt.Println(resp)
		} else {
			parts := strings.SplitN(resp, ":", 2)
			if len(parts) == 2 {
				switch parts[0] {
				case "S":
					process_status(strings.TrimRight(parts[1], " \r\n"))
				case "B":
					process_battery(strings.TrimRight(parts[1], " \r\n"))
				case "I":
					fmt.Println(strings.TrimRight(parts[1], " \r\n"))
				}
			} else {
				log.Printf("Invalid response: %q", resp)
			}
		}
	} else {
		log.Println(err)
	}
	return
}

func do_sens(args []string, p ...interface{}) (stop bool) {
	ms := p[0].(*ImmState)
	hostid, err := strconv.Atoi(ms.peer_sn)
	if err != nil {
		log.Fatal(err)
	}

	if len(args) == 0 {
		fmt.Println("Usage: sens name [name ...]")
		return
	}

	if args[0] == "--raw" {
		resp, err := ms.imm.HostMessage(hostid,
			"sens "+strings.Join(args[1:], " "))
		if err != nil {
			log.Println(err)
		} else {
			fmt.Println(resp)
		}
	} else {
		resp, err := ms.imm.HostMessage(hostid,
			"sens "+strings.Join(args, " "))
		if err != nil {
			log.Println(err)
		} else {
			parts := strings.SplitN(resp, ":", 2)
			if len(parts) == 2 {
				switch parts[0] {
				case "D":
					process_sensor(strings.TrimRight(parts[1], " \r\n"))
				case "I":
					fmt.Println(strings.TrimRight(parts[1], " \r\n"))
				}
			} else {
				log.Printf("Invalid response: %q", resp)
			}
		}
	}
	return
}

func do_prf(args []string, p ...interface{}) (stop bool) {
	ms := p[0].(*ImmState)
	hostid, err := strconv.Atoi(ms.peer_sn)
	if err != nil {
		log.Fatal(err)
	}

	// Extract Profile parameters from the command line.
	attrs := make(map[string]interface{})
	for _, v := range args {
		f := strings.Split(v, "=")
		ival, err := strconv.ParseInt(f[1], 0, 32)
		if err == nil {
			attrs[f[0]] = int(ival)
		} else {
			attrs[f[0]] = f[1]
		}
	}

	prf := dpcmsg.ProfileFromInterface(attrs)
	switch prf.GetDir() {
	case dpcmsg.Profile_STATIONARY:
		prf.Depth = proto.Int32(0)
	case dpcmsg.Profile_DOCKING:
		prf.Depth = proto.Int32(0)
		prf.Maxtime = proto.Int32(0)
	}

	data, err := proto.Marshal(prf)
	if err == nil {
		resp, err := ms.imm.HostMessage(hostid,
			"prf "+base64.StdEncoding.EncodeToString(data))
		if err == nil {
			parts := strings.SplitN(resp, ":", 2)
			if len(parts) > 1 {
				fmt.Println(strings.TrimRight(parts[1], " \r\n"))
			}
		} else {
			log.Println(err)
		}
	} else {
		log.Println(err)
	}

	return
}

func do_pattern(args []string, p ...interface{}) (stop bool) {
	ms := p[0].(*ImmState)
	hostid, err := strconv.Atoi(ms.peer_sn)
	if err != nil {
		log.Fatal(err)
	}

	if len(args) == 0 {
		fmt.Println("Missing pattern name")
		return
	}

	resp, err := ms.imm.HostMessage(hostid, "pattern "+args[0])
	if err == nil {
		parts := strings.SplitN(resp, ":", 2)
		if len(parts) > 1 {
			fmt.Println(strings.TrimRight(parts[1], " \r\n"))
		}
	} else {
		log.Println(err)
	}

	return
}

func do_get(args []string, p ...interface{}) (stop bool) {
	ms := p[0].(*ImmState)
	hostid, err := strconv.Atoi(ms.peer_sn)
	if err != nil {
		log.Fatal(err)
	}

	if len(args) == 0 {
		fmt.Println("Usage: get DBKEY")
		return
	}

	resp, err := ms.imm.HostMessage(hostid, "get "+args[0])
	if err == nil {
		parts := strings.SplitN(resp, ":", 2)
		if len(parts) == 2 {
			switch parts[0] {
			case "E":
				process_dbentry(strings.TrimRight(parts[1], " \r\n"))
			case "I":
				fmt.Println(strings.TrimRight(parts[1], " \r\n"))
			}
		}
	} else {
		log.Println(err)
	}

	return
}

func do_set(args []string, p ...interface{}) (stop bool) {
	ms := p[0].(*ImmState)
	hostid, err := strconv.Atoi(ms.peer_sn)
	if err != nil {
		log.Fatal(err)
	}

	if len(args) < 2 {
		fmt.Println("Usage: set DBKEY VALUE...")
		return
	}

	e := dpcmsg.DbEntry{Key: &args[0], Value: args[1:]}

	data, err := proto.Marshal(&e)
	if err == nil {
		resp, err := ms.imm.HostMessage(hostid,
			"set "+base64.StdEncoding.EncodeToString(data))
		if err == nil {
			parts := strings.SplitN(resp, ":", 2)
			if len(parts) > 1 {
				fmt.Println(strings.TrimRight(parts[1], " \r\n"))
			}
		} else {
			log.Println(err)
		}
	} else {
		log.Println(err)
	}

	return
}

func do_go(args []string, p ...interface{}) (stop bool) {
	ms := p[0].(*ImmState)
	hostid, err := strconv.Atoi(ms.peer_sn)
	if err != nil {
		log.Fatal(err)
	}

	resp, err := ms.imm.HostMessage(hostid, "go")
	if err == nil {
		parts := strings.SplitN(resp, ":", 2)
		if len(parts) > 1 {
			fmt.Println(strings.TrimRight(parts[1], " \r\n"))
		}
	} else {
		log.Println(err)
	}

	return
}

func run_shell(ms *ImmState) {
	shell := &cmd.Cmd{Prompt: "DPC> "}
	shell.Init()

	shell.Add(cmd.Command{
		Name: "ping",
		Help: `Test the communication link`,
		Call: func(line string) bool {
			args := strings.Split(line, " ")
			return do_ping(args, ms)
		}})

	shell.Add(cmd.Command{
		Name: "reboot",
		Help: `
Argument: delay
Reboot the DPC after the specified delay`,
		Call: func(line string) bool {
			args := strings.Split(line, " ")
			return do_reboot(args, ms)
		}})

	shell.Add(cmd.Command{
		Name: "get",
		Help: `
Argument: dbkey
Lookup a DPC database entry`,
		Call: func(line string) bool {
			args := strings.Split(line, " ")
			return do_get(args, ms)
		}})

	shell.Add(cmd.Command{
		Name: "set",
		Help: `
Argument: dbkey value...
Set a DPC database entry`,
		Call: func(line string) bool {
			args := strings.Split(line, " ")
			return do_set(args, ms)
		}})

	shell.Add(cmd.Command{
		Name: "exit",
		Help: `Exit the application`,
		Call: func(line string) bool {
			return true
		}})

	shell.Add(cmd.Command{
		Name: "status",
		Help: `Return the DPC status`,
		Call: func(line string) bool {
			args := strings.Split(line, " ")
			return do_status(args, ms)
		}})

	shell.Add(cmd.Command{
		Name: "batt",
		Help: `Return the most recent DPC battery status`,
		Call: func(line string) bool {
			args := strings.Split(line, " ")
			return do_batt(args, ms)
		}})

	shell.Add(cmd.Command{
		Name: "sens",
		Help: `
Arguments: name1 [name2 ... ]
Return the most recent sample from one or more sensors`,
		Call: func(line string) bool {
			args := strings.Split(line, " ")
			return do_sens(args, ms)
		}})

	shell.Add(cmd.Command{
		Name: "profile",
		Help: `
Arguments: param=val [param=val ...]
Create a new Profile description and transfer to the DPC`,
		Call: func(line string) bool {
			args := strings.Split(line, " ")
			return do_prf(args, ms)
		}})

	shell.Add(cmd.Command{
		Name: "pattern",
		Help: `
Arguments: name
Add a pattern to the profile queue`,
		Call: func(line string) bool {
			args := strings.Split(line, " ")
			return do_pattern(args, ms)
		}})

	shell.Add(cmd.Command{
		Name: "go",
		Help: `Start processing the profile queue`,
		Call: func(line string) bool {
			args := strings.Split(line, " ")
			return do_go(args, ms)
		}})

	shell.Add(cmd.Command{
		Name: "cap",
		Help: `Capture the line`,
		Call: func(line string) bool {
			err := ms.imm.CaptureLine(true)
			if err != nil {
				log.Println(err)
				return true
			}
			return false
		}})

	shell.Add(cmd.Command{
		Name: "debug",
		Help: `
Arguments: on | off
Enable or disable debug mode`,
		Call: func(line string) bool {
			args := strings.Split(line, " ")
			if len(args) > 0 {
				if args[0] == "on" {
					ms.imm.SetDebug(true)
				} else {
					ms.imm.SetDebug(false)
				}
			}
			return false
		}})

	shell.CmdLoop()
}
