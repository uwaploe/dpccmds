// Process messages from the IMM communication interface.
package main

import (
	"bitbucket.org/uwaploe/go-dpcmsg"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/gogo/protobuf/proto"
	"log"
	"time"
)

// Process the reply to a sensor data request
func process_sensor(rawdata string) {
	cont := dpcmsg.SensorData{}
	enc := base64.StdEncoding

	data, err := enc.DecodeString(rawdata)
	if err != nil {
		log.Println(err)
		return
	}

	err = proto.Unmarshal(data, &cont)
	if err != nil {
		log.Println(err)
		return
	}

	// Do not process data records older than the last sample
	for _, rec := range cont.ToDataRecords() {
		s, err := json.MarshalIndent(rec.Data, "", "    ")
		if err != nil {
			log.Printf("JSON conversion failed: %v\n", err)
		} else {
			fmt.Printf("%s\n", rec.T.Format(time.RFC3339))
			fmt.Printf("Sensor: %s\n", rec.Source)
			fmt.Printf("%s\n", s)
		}
	}
}

// Process the reply to a status request
func process_status(rawdata string) {
	rec := &dpcmsg.Status{}
	enc := base64.StdEncoding

	data, err := enc.DecodeString(rawdata)
	if err != nil {
		log.Println(err)
		return
	}

	err = proto.Unmarshal(data, rec)
	if err != nil {
		log.Println(err)
		return
	}

	pr := rec.ToDataRecord()
	s, err := json.MarshalIndent(pr.Data, "", "    ")
	if err != nil {
		log.Printf("JSON conversion failed: %v\n", err)
	} else {
		fmt.Printf("%s\n", pr.T.Format(time.RFC3339))
		fmt.Printf("%s\n", s)
	}
}

// Process the reply to a battery request
func process_battery(rawdata string) {
	rec := &dpcmsg.Battery{}
	enc := base64.StdEncoding

	data, err := enc.DecodeString(rawdata)
	if err != nil {
		log.Println(err)
		return
	}

	err = proto.Unmarshal(data, rec)
	if err != nil {
		log.Println(err)
		return
	}

	dr := rec.ToDataRecord()
	s, err := json.MarshalIndent(dr.Data, "", "    ")
	if err != nil {
		log.Printf("JSON conversion failed: %v\n", err)
	} else {
		fmt.Printf("%s\n", dr.T.Format(time.RFC3339))
		fmt.Printf("%s\n", s)
	}
}

// Process the reply to a database lookup
func process_dbentry(rawdata string) {
	rec := &dpcmsg.DbEntry{}
	enc := base64.StdEncoding

	data, err := enc.DecodeString(rawdata)
	if err != nil {
		log.Println(err)
		return
	}

	err = proto.Unmarshal(data, rec)
	if err != nil {
		log.Println(err)
		return
	}

	fmt.Printf("%q=%q\n", rec.GetKey(), rec.GetValue())
}
